// SPDX-FileCopyrightText: 2023 - 2024 Dusan Mijatovic (Netherlands eScience Center)
// SPDX-FileCopyrightText: 2023 - 2024 Netherlands eScience Center
// SPDX-FileCopyrightText: 2024 Christian Meeßen (GFZ) <christian.meessen@gfz-potsdam.de>
// SPDX-FileCopyrightText: 2024 Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences
//
// SPDX-License-Identifier: Apache-2.0
// SPDX-License-Identifier: EUPL-1.2

import Button from '@mui/material/Button'

export default function LinkOrcidButton({orcidAuthLink,disabled}:{orcidAuthLink:string|null,disabled:boolean}) {
  if (orcidAuthLink){
    return (
      <Button
        disabled={disabled}
        href={orcidAuthLink}
        variant="contained"
        color="info"
        sx={{
          borderRadius: '40px',
          letterSpacing: '0.01rem',
          '&:hover': {
            backgroundColor: '#22b9f8',
            color: '#fff'
          }
        }}
      >
          Link your ORCID account
      </Button>
    )
  }
  return null
}
