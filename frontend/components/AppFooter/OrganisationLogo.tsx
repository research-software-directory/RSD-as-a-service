// SPDX-FileCopyrightText: 2022 Dusan Mijatovic (dv4all)
// SPDX-FileCopyrightText: 2022 dv4all
// SPDX-FileCopyrightText: 2023 - 2025 Christian Meeßen (GFZ) <christian.meessen@gfz-potsdam.de>
// SPDX-FileCopyrightText: 2023 - 2025 Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences
// SPDX-FileCopyrightText: 2025 Paula Stock (GFZ) <paula.stock@gfz.de>
//
// SPDX-License-Identifier: Apache-2.0
// SPDX-License-Identifier: EUPL-1.2

import {RsdHost} from '~/config/rsdSettingsReducer'
import LogoAvatar from '../layout/LogoAvatar'

function GfzLogo() {
  return (
    <a href='https://gfz.de' target='_blank'
      className="h-[6rem] w-[26rem]" rel="noreferrer">
      <LogoAvatar
        name='GFZ Potsdam'
        src='/images/GFZ-Wortbildmarke-DE-weiss.svg
        '
        sx={{
          '& img' : {
            maxHeight: '100%',
            maxWidth: '100%',
            objectFit: 'contain'
          }
        }}
      />
    </a>
  )
}

export default function OrganisationLogo({host}: { host: RsdHost }) {

  if (host?.logo_url && host?.website) {
    const {name,logo_url,website}=host
    return (
      <div className="flex flex-wrap flex-col gap-[1rem]">
        <a href={website} target="_blank"
          className="h-[4rem] w-[16rem]" rel="noreferrer">
          <LogoAvatar
            name={name}
            src={logo_url}
            sx={{
              '& img' : {
                maxHeight: '100%',
                maxWidth: '100%',
                objectFit: 'contain'
              }
            }}
          />
        </a>
        <GfzLogo />
      </div>
    )
  }
  if (host?.logo_url) {
    const {name,logo_url}=host
    return (
      <div className="flex flex-wrap items-center gap-[1rem]">
        <span className="h-[4rem] w-[16rem]">
          <LogoAvatar
            name={name}
            src={logo_url}
          />
        </span>
        <GfzLogo />
      </div>
    )
  }
  return null
}
