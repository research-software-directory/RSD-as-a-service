---
slug: /
title: Introduction
description: This is short description
sidebar_position: 1
sidebar_label: Introduction
---

Welcome to the documentation of the [Helmholtz Research Software Directory](https://helmholtz.software)!

Research software is one of the most important scientific instruments available today. Data-intensive research and computational simulation are the foundation of many scientific
discoveries.

The Helmholtz Research Software Directory (RSD) is an online platform designed to showcase the impact of research software that is being developed within the [Helmholtz Association of Research Centres](https://helmholtz.de) on research and society. By showing research software together with
relevant contextual information such as scientific publications, contributors, projects, citation information and much more, we stimulate the reuse and encourage proper citation
to ensure the researchers and RSEs developing the software get credit for their work.

The Helmholtz RSD is a fork of the RSD by the [Netherlands eScience Center](https://esciencecenter.nl). More information about the concepts behind the RSD can be found in this excellent
[blog](https://blog.esciencecenter.nl/the-research-software-directory-and-how-it-promotes-software-citation-4bd2137a6b8).

## About the RSD

The Research Software Directory was initiated in 2017 by the [Netherlands eScience Center](https://www.esciencecenter.nl/) as a platform to showcase the research software developed at
our center. After successfully using the RSD for several years, we decided in 2021 to re-engineer the platform, so we can offer it as an online service to other research
organisations. In 2022, our development effort was joined by [HIFIS](https://hifis.net/).
