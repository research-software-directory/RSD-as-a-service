# You want to contribute in some other way

Contributions to the code are by no means the only way to contribute to the Research Software Directory. If you wish to contribute in some other way, please contact us at [support@hifis.net](mailto:support@hifis.net?subject=[RSD]).
