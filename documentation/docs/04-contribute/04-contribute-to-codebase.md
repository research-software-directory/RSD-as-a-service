# You want to make change in RSD code base

Please have a look at our [contributing guidelines](https://codebase.helmholtz.cloud/research-software-directory/RSD-as-a-service/-/blob/main/CONTRIBUTING.md?ref_type=heads#you-want-to-make-some-kind-of-change-to-the-code-base-youself).
