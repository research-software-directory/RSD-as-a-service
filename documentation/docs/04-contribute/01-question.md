# You have a question or suggestion

For general questions (not directly related to the development of the software) you can send an email to [support@hifis.net](mailto:support@hifis.net?subject=[RSD]). Alternatively, you can also submit an issue:

1. use the search functionality in our [Helmholtz GitLab repository](https://codebase.helmholtz.cloud/research-software-directory/RSD-as-a-service/-/issues) or in the upstream [GitHub repo by the NLeSC](https://github.com/research-software-directory/RSD-as-a-service/issues) to see if someone already filed the same issue;
2. if you find a similar issue, you can add your own comments to this issue;
3. if your issue search did not yield any relevant results, make a new issue;
4. apply the `question` label; apply other labels when relevant.
