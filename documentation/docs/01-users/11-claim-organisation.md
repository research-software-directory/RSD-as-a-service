# Claim organisation

Organisations have the opportunity to maintain their representation within the Helmholtz RSD.
In order to achieve this, official representatives of the organisation can take on the role of maintainers of their organisation.
This possibility extends to both Helmholtz organisations and other organisations alike.

## How to claim an organisation

If you would like to claim an organisation, please contact us via [support@hifis.net](mailto:support@hifis.net?subject=[RSD]%20Claiming%20an%20organisation) and clearly provide the following information:

* Your name
* Which organisation you represent

We will then contact you to arrange access for your organisation.
We may reserve the right to demand proof for the right to manage the representation of your organisation (see Art. 4.4 in our [Terms of Service](https://helmholtz.software/page/terms-of-service)).

## Which organisations can join the RSD

In the RSD, we interpret the term "organisation" very broadly. Any organisation involved in the development of research software should be able to join the RSD.
Examples of organisations include (but are not limited to):

* Universities
* Research Institutes
* Research Infrastructure Projects
* Virtual Research Organisations and Consortia
* Research Communities
* Startups and companies which maintain or contribute to research software
* etc.
