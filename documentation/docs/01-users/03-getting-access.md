
# How to get access

While anyone is free to browse through the content of the RSD, you'll need to be able to sign in before you can add our own content.

As an employee of an Helmholtz Organisation, you can sign in using your institutional account via [Helmholtz AAI](https://hifis.net/aai/).

If you are employed by a non-Helmholtz organisation that is listed in the RSD, and you wish to maintain your organisation, please look at how to [claim an organisation](/users/claim-organisation).

## How to sign in

### Getting access with your Helmholtz account

To sign in to the RSD, go to the __"Sign in"__ button at the top right corner of the page. You will be forwarded to the Helmholtz AAI:

![Helmholtz AAI selection dialog](img/aai-organisation-selection.png)

Find your institution by typing in the name in the search bar.
You will be forwarded to your centre's identity provider and can now log in using your credentials.
If you sign in to the RSD for the first time, the identify provider may ask you permission to share information with the RSD.

After providing you credentials you will return to the RSD page you came from.

### Getting access as non-Helmholtz user

To obtain access as a non-Helmholtz user, please contact us via [support@hifis.net](mailto:support@hifis.net?subject=[RSD]%20Access%20to%20helmholtz.software).

## How to sign out

To sign out, you can use the profile button in the top right corner of the page and select __"Logout"__:

![video](img/rsd-logout.gif)
