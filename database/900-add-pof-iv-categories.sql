-- SPDX-FileCopyrightText: 2023 Felix Mühlbauer (GFZ) <felix.muehlbauer@gfz-potsdam.de>
-- SPDX-FileCopyrightText: 2023 Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences
--
-- SPDX-License-Identifier: Apache-2.0

-- THIS FILE SHOULD BE MOVED TO THE DEPLOMENT REPO AND USED AS A MIGRATION FILE

CREATE FUNCTION add_hgf_categories()
RETURNS TABLE (LIKE category)
LANGUAGE plpgsql
AS $$
	DECLARE hgf_pof_iv uuid;
	DECLARE parent2 uuid;
	DECLARE parent3 uuid;
BEGIN
	INSERT INTO category(short_name, name, properties) VALUES
		(
			'Helmholtz PoF IV',
			'Helmholtz Program-oriented Funding IV',
			'{
				"is_highlight": true,
				"description": "Use this set of categories to indicate to which Program-oriented Funding (PoF) IV topic your software belongs to. PoF IV runs from 2021 to 2027.",
				"subtitle": "Add relevant PoF IV topics.",
				"tree_level_labels":
					[
						"Research Field",
						"Research Program",
						"PoF Topic"
					]
				}'::json
		)
		RETURNING id INTO hgf_pof_iv;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			hgf_pof_iv,
			'Energy',
			'1 Energy'
		)
		RETURNING id INTO parent2;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent2,
			'Energy System Design',
			'1.1 Energy System Design'
		)
		RETURNING id INTO parent3;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Energy System Transformation',
			'1.1.1 Energy System Transformation'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Digitalization and System Technology',
			'1.1.2 Digitalization and System Technology'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent2,
			'Materials and Technologies for the Energy Transition',
			'1.2 Materials and Technologies for the Energy Transition'
		)
		RETURNING id INTO parent3;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Photovoltaics and Wind Energy',
			'1.2.1 Photovoltaics and Wind Energy'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Electrochemical Energy Storage',
			'1.2.2 Electrochemical Energy Storage'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Chemical Energy Carriers',
			'1.2.3 Chemical Energy Carriers'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'High-Temperature Thermal Technologies',
			'1.2.4 High-Temperature Thermal Technologies'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Resource and Energy Efficiency',
			'1.2.5 Resource and Energy Efficiency'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent2,
			'Fusion',
			'1.3 Fusion'
		)
		RETURNING id INTO parent3;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Stellarator Research',
			'1.3.1 Stellarator Research'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Tokamak Physics',
			'1.3.2 Tokamak Physics'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Fusion Technologies and Materials',
			'1.3.3 Fusion Technologies and Materials'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Plasma-Wall Interactions',
			'1.3.4 Plasma-Wall Interactions'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent2,
			'Nuclear Waste Management, Safety and Radiation Research',
			'1.4 Nuclear Waste Management, Safety and Radiation Research'
		)
		RETURNING id INTO parent3;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Nuclear Waste Management',
			'1.4.1 Nuclear Waste Management'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Reactor Safety',
			'1.4.2 Reactor Safety'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			hgf_pof_iv,
			'Earth and Environment',
			'2 Earth and Environment'
		)
		RETURNING id INTO parent2;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent2,
			'The Changing Earth - Sustaining our Future',
			'2.1 The Changing Earth - Sustaining our Future'
		)
		RETURNING id INTO parent3;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'The Atmosphere in Global Change',
			'2.1.1 The Atmosphere in Global Change'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Ocean and Cryosphere in Climate',
			'2.1.2 Ocean and Cryosphere in Climate'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Living on a Restless Earth',
			'2.1.3 Living on a Restless Earth - Towards Forecasting Geohazards'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Coastal Transistion Zones under Natural and Human Pressure',
			'2.1.4 Coastal Transistion Zones under Natural and Human Pressure'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Landscapes of the Future',
			'2.1.5 Landscapes of the Future: Securing Terrestrial Ecosystems and Freshwater Ressources'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Marine and Polar Life',
			'2.1.6 Marine and Polar Life: Sustaining Biodiversity, Biotic Interactions and Biogeochemical Functions'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Towards a Sustainable Bioeconomy',
			'2.1.7 Towards a Sustainable Bioeconomy - Resources, Utilization, Engineering and AgroEcosystems'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Georeseources for the Energy Transition and a High-Tech Society',
			'2.1.8 Georeseources for the Energy Transition and a High-Tech Society'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'One Healthy Planet',
			'2.1.9 One Healthy Planet - Developing an Exposome Approach to Integrate Human and Environmental Hazard Assessment'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Neumayer Station III',
			'2 Neumayer Station III'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Polar Aircraft',
			'2 Polar Aircraft'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Marine Stations Helgoland and Sylt',
			'2 Marine Stations Helgoland and Sylt'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'RV Polarstern',
			'2 RV Polarstern'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'RV Heincke', '2 RV Heincke'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'RV Alkor and Ocean Robotics',
			'2 RV Alkor and Ocean Robotics'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Modular Earth Science Infrastructure (MESI) (GFZ)',
			'2 Modular Earth Science Infrastructure (MESI) (GFZ)'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			hgf_pof_iv,
			'Health',
			'3 Health'
		)
		RETURNING id INTO parent2;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent2,
			'Cancer Research',
			'3.1 Cancer Research'
		)
		RETURNING id INTO parent3;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Cell and Tumor Biology',
			'3.1.1 Cell and Tumor Biology'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Functional and Structural Genomics',
			'3.1.2 Functional and Structural Genomics'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Cancer Risk Factors and Prevention',
			'3.1.3 Cancer Risk Factors and Prevention'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Immunology and Cancer',
			'3.1.4 Immunology and Cancer'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Imaging and Radiooncology',
			'3.1.5 Imaging and Radiooncology'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Infection, Inflammation and Cancer',
			'3.1.6 Infection, Inflammation and Cancer'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent2,
			'Environmental and Metabolic Health',
			'3.2 Environmental and Metabolic Health'
		)
		RETURNING id INTO parent3;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Metabolic Health',
			'3.2.1 Metabolic Health'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Environmental Health',
			'3.2.2 Environmental Health'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Molecular Targets and Therapies',
			'3.2.3 Molecular Targets and Therapies'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Cell Programming and Repair',
			'3.2.4 Cell Programming and Repair'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Bioengineering',
			'3.2.5 Bioengineering'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Computational Health',
			'3.2.6 Computational Health'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent2,
			'Systems Medicine and Cardiovascular Diseases',
			'3.3 Systems Medicine and Cardiovascular Diseases'
		)
		RETURNING id INTO parent3;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Genes, Cells and Cell-based Medicine',
			'3.3.1 Genes, Cells and Cell-based Medicine'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Molecular Processes and Therapies',
			'3.3.2 Molecular Processes and Therapies'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Integrative Biomedicine',
			'3.3.3 Integrative Biomedicine'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent2,
			'Infection Research',
			'3.4 Infection Research'
		)
		RETURNING id INTO parent3;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Bacterial and Viral Pathogens',
			'3.4.1 Bacterial and Viral Pathogens'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Immune Response and Interventions',
			'3.4.2 Immune Response and Interventions'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Anti-infectives', '3.4.3 Anti-infectives'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent2,
			'Neurodegenerative Diseases',
			'3.5 Neurodegenerative Diseases'
		)
		RETURNING id INTO parent3;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Brain Function',
			'3.5.1 Brain Function'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Disease Mechanisms',
			'3.5.2 Disease Mechanisms'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Clinical and Health Care Research',
			'3.5.3 Clinical and Health Care Research'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Disease Prevention and Healthy Aging',
			'3.5.4 Disease Prevention and Healthy Aging'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'National Cohort',
			'3 National Cohort'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			hgf_pof_iv,
			'Aeronautics, Space and Transport',
			'4 Aeronautics, Space and Transport'
		)
		RETURNING id INTO parent2;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent2,
			'Aeronautics',
			'4.1 Aeronautics'
		)
		RETURNING id INTO parent3;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Efficient Vehicle',
			'4.1.1 Efficient Vehicle'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Components & Systems',
			'4.1.2 Components & Systems'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Clean Propulsion',
			'4.1.3 Clean Propulsion'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Air Transportation and Impact',
			'4.1.4 Air Transportation and Impact'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent2,
			'Space',
			'4.2 Space'
		)
		RETURNING id INTO parent3;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Earth Observation',
			'4.2.1 Earth Observation'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Communication, Navigation and Quantum Technology',
			'4.2.2 Communication, Navigation and Quantum Technology'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Space Exploration',
			'4.2.3 Space Exploration'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Research under Space Conditions',
			'4.2.4 Research under Space Conditions'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Space Transportation',
			'4.2.5 Space Transportation'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Space System Technology',
			'4.2.6 Space System Technology'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Robotics',
			'4.2.7 Robotics'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent2,
			'Transport',
			'4.3 Transport'
		)
		RETURNING id INTO parent3;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Road Transport',
			'4.3.1 Road Transport'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Rail Transport',
			'4.3.2 Rail Transport'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Transport System',
			'4.3.3 Transport System'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			hgf_pof_iv,
			'Information',
			'5 Information'
		)
		RETURNING id INTO parent2;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent2,
			'Engineering Digital Futures',
			'5.1 Engineering Digital Futures: Supercomputing, Data Management and Information Security for Knowledge and Action'
		)
		RETURNING id INTO parent3;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Enabling Computational- & Data-Intensive Science and Engineering',
			'5.1.1 Enabling Computational- & Data-Intensive Science and Engineering'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Supercomputing & Big Data Infrastructures',
			'5.1.2 Supercomputing & Big Data Infrastructures'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Engineering Secure Systems',
			'5.1.3 Engineering Secure Systems'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Knowledge for Action',
			'5.1.4 Knowledge for Action'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent2,
			'Natural, Artificial and Cognitive Information Processing',
			'5.2 Natural, Artificial and Cognitive Information Processing'
		)
		RETURNING id INTO parent3;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Quantum Materials',
			'5.2.1 Quantum Materials'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Quantum Computing',
			'5.2.2 Quantum Computing'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Neuromorphic Computing and Network Dynamics',
			'5.2.3 Neuromorphic Computing and Network Dynamics'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Molecular and Cellular Information Processing',
			'5.2.4 Molecular and Cellular Information Processing'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Decoding Brain Organization and Dysfunction',
			'5.2.5 Decoding Brain Organization and Dysfunction'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent2,
			'Materials Systems Engineering',
			'5.3 Materials Systems Engineering'
		)
		RETURNING id INTO parent3;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Functionality by Information-Guided Design',
			'5.3.1 Functionality by Information-Guided Design: From Molecular Concepts to Materials'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Optics & Photonics',
			'5.3.2 Optics & Photonics: Materials, Devices, and Systems'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Adaptive and Bioinstructive Materials Systems',
			'5.3.3 Adaptive and Bioinstructive Materials Systems'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Scale-Bridging Designed Materials',
			'5.3.4 Scale-Bridging Designed Materials: From Fundamentals to Systems'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Materials Information Discovery',
			'5.3.5 Materials Information Discovery'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			hgf_pof_iv,
			'Matter',
			'6 Matter'
		)
		RETURNING id INTO parent2;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent2,
			'Matter and the Universe',
			'6.1 Matter and the Universe'
		)
		RETURNING id INTO parent3;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Fundamental Particles and Forces',
			'6.1.1 Fundamental Particles and Forces'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Cosmic Matter in the Laboratory',
			'6.1.2 Cosmic Matter in the Laboratory'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Matter and Radiation from the Universe',
			'6.1.3 Matter and Radiation from the Universe'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent2,
			'Matter and Technologies',
			'6.2 Matter and Technologies'
		)
		RETURNING id INTO parent3;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Accelerator Research and Development',
			'6.2.1 Accelerator Research and Development'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Detector Technologies and Systems',
			'6.2.2 Detector Technologies and Systems'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Data Management and Analysis',
			'6.2.3 Data Management and Analysis'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent2,
			'From Matter to Materials and Life',
			'6.3 From Matter to Materials and Life'
		)
		RETURNING id INTO parent3;

	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Matter - Dynamics, Mechanism and Control',
			'6.3.1 Matter - Dynamics, Mechanism and Control'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Materials - Quantum, Complex and Functional Materials',
			'6.3.2 Materials - Quantum, Complex and Functional Materials'
		);
	INSERT INTO category(parent, short_name, name) VALUES
		(
			parent3,
			'Life - Building Blocks of Life: Structure and Function',
			'6.3.3 Life - Building Blocks of Life: Structure and Function'
		);
END
$$;

SELECT add_hgf_categories();
DROP FUNCTION add_hgf_categories;
