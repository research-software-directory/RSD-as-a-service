<!--
SPDX-FileCopyrightText: 2023 Christian Meeßen (GFZ) <christian.meessen@gfz-potsdam.de>
SPDX-FileCopyrightText: 2023 Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences

SPDX-License-Identifier: CC-BY-4.0
-->

- `CITATION.cff`
  - [ ] Version number
  - [ ] Release date
  - [ ] Check upstream version number in references
- [ ] Create release notes

# Release notes

### What's changed

### HIFIS specific changes

#### Features

#### Bug fixes

### Incoming changes from upstream vX.X.X
