#!/bin/bash

# SPDX-FileCopyrightText: 2022 - 2024 Christian Meeßen (GFZ) <christian.meessen@gfz-potsdam.de>
# SPDX-FileCopyrightText: 2022 - 2024 Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences
# SPDX-FileCopyrightText: 2022 Jason Maassen (Netherlands eScience Center) <j.maassen@esciencecenter.nl>
# SPDX-FileCopyrightText: 2022 Netherlands eScience Center
#
# SPDX-License-Identifier: Apache-2.0
# SPDX-License-Identifier: EUPL-1.2

AUTHOR=
EMAIL=
ORGANISATION=
# If your organisation's name is very long, put an abbreviation here, otherwise fill in the same value as for ORGANISATION
# e.g. "Helmholtz Centre Potsdam - GFZ German Research Centre for Geosciences" could become "GFZ".
# This is the value that will be displayed after your name, whereas ORGANISATION will be displayed on a separate line
ORGANISATION_SHORT=
# End of configuration

function check_program_exists () {
    if ! command -v $1 &> /dev/null; then
        echo -e "\nWARNING: pre-commit could not find the program $1. The commit was performed nonetheless.\n"
        exit 0
    fi
}

# Check whether we are in a merge commit
merge_check=$(git rev-parse -q --verify MERGE_HEAD)
if [[ ${merge_check} ]]; then
    # We are in a merge commit and do not want to alter this
    exit 0
fi

check_program_exists reuse
check_program_exists date
check_program_exists dirname

YEAR=$(date +"%Y")
AUTHOR_STRING=$AUTHOR
if [[ "ORGANISATION_SHORT" != "" ]]; then
    AUTHOR_STRING="${AUTHOR_STRING} (${ORGANISATION_SHORT})"
fi
if [[ "$EMAIL" != "" ]]; then
    AUTHOR_STRING="${AUTHOR_STRING} <${EMAIL}>"
fi

BASE_ARGS="--year $YEAR --merge-copyrights"
if [[ $ORGANISATION != "" ]]; then
    BASE_ARGS="${BASE_ARGS} --copyright \"${ORGANISATION}\""
fi
if [[ $AUTHOR != "" ]]; then
    BASE_ARGS="${BASE_ARGS} --copyright \"${AUTHOR_STRING}\""
fi

if [[ $1 == "-d" || $1 == "--dry-run" ]]; then
    echo "Your copyright strings are:"
    echo "Author: ${AUTHOR_STRING}"
    echo "Organisation: ${ORGANISATION}"
    exit 0
fi

OIFS=${IFS}
IFS=$'\n'
declare -a STAGED_FILES=( $(git diff --name-only --cached) )

for file in ${STAGED_FILES[@]}; do
    file_path=$(dirname ${file})
    if [[ ${file_path} == "LICENSES" ]]; then
        echo "Info: did not auto-assign a license to ${file}, because it is located in the LICENSES directory."
        continue
    fi
    file_extension="${file##*.}"
    case ${file_extension} in
        license)
            continue
            ;;
        md | webp | png | jpg | jpeg | svg | json)
            LICENSE="CC-BY-4.0"
            ;;
        conf | cron | lock | gitignore | example )
            LICENSE="CC0-1.0"
            ;;
        *)
            LICENSE="EUPL-1.2"
            ;;
    esac
    ARGS="${BASE_ARGS} --license $LICENSE"
    eval "reuse annotate $ARGS $file 1> /tmp/reuse_out 2> /tmp/reuse_error"
    case $? in
        0)
            if ! git diff --quiet $file; then
                git add $file
                echo "INFO: Added changes by \"${AUTHOR}\" to ${file} under license ${LICENSE}."
            fi
            continue
            ;;
        2)
            echo -e "\nWARNING: Did not update ${file}, because file type could not be recognised.\n"
            continue
            ;;
        *)
            echo "\nWARNING: An unhandled error code occurred for file ${file}.\n"
            continue
            ;;
    esac
done
